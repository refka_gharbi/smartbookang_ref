import { GererCompteComponent } from './gerer-compte/gerer-compte.component';
import { ListeLivresComponent } from './liste-livres/liste-livres.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'listeLivres', component:ListeLivresComponent},
  {path:'gererCompte', component:GererCompteComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
